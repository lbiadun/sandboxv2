/*
 * Session.cpp
 *
 *  Created on: 10 mar 2016
 *      Author: lukasz
 */

#include <SandboxException.h>
#include <FileHelper.h>
#include <Config.h>
#include <Log.h>

#include <time.h>
#include <iostream>
#include <fstream>
#include <sys/types.h>
#include <sys/stat.h>

#include <uuid/uuid.h>
#include <json/json.h>
#include <boost/algorithm/string.hpp>
#include <mysql/cppconn/prepared_statement.h>

#include <Session.h>


Session::Session(Manager::SandboxCopy &sand, Manager::DbConn db_conn)
{
	id_sand = sand.uuid;
	ip = sand.ip;
	port = sand.port;

	uuid_t id;
	uuid_generate(id);

	uuid = new char[100];
	uuid_unparse(id, uuid);

	tmp_dir = TMP_DIR "/";
	tmp_dir += uuid;
	mkdir(tmp_dir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

	driver = sql::mysql::get_mysql_driver_instance();
	con = driver->connect(std::string("tcp://") + db_conn.host + ":3306", db_conn.user, db_conn.pass);

	if( !con->isValid() )
	{
		throw SandboxException(201, "Manager:", "database not open");
	}

	stmt = con->createStatement();
	stmt->execute(std::string("USE ") + db_conn.name );
	res = NULL;
}

Session::~Session()
{
	std::string command = "rm -r ";
	command.append(tmp_dir);
	system(command.c_str());

	delete res;
	delete stmt;
	delete con;
	delete prep_stmt;
}

void Session::processRequest(ServerSocket &sock)
{
	std::string content = captureContent(sock);

	Session::Task task = processJsonContent(content);
	Session::Lang lang = generateMakefile(task);
	int n = generateFilesInput(task);
	generateConfigFile(task, lang, n);
	generateMainFile(task, lang);
	ClientSocket client_sock(ip, port);
	prepareAndSendPackage(client_sock, lang, n);
	captureResponse(client_sock);
	int r = logResult(sock, task, lang, n);
	prepareAndSendResponse(sock, r);
}

std::string Session::captureContent(ServerSocket &sock)
{
	std::string data;
	std::string end_header = "\r\n\r\n";

	sock >> data;
	int pos = data.find(end_header);
	std::string content = data.substr(pos + end_header.length());

	return content;
}

std::string Session::getJsonString(Json::Value value, std::string name)
{
	if( !value.isMember(name) || !value.get(name, "").isString() )
	{
		std::string msg = name  + " in json file does not exist or is in the wrong format";
		throw SandboxException(204, "Manager", msg.c_str());
	}

	return value.get(name, "").asString();
}

int Session::getJsonInteger(Json::Value value, std::string name)
{
	if( !value.isMember(name) || !value.get(name, 0).isInt() )
	{
		std::string msg = name  + " in json file does not exist or is in the wrong format";
		throw SandboxException(204, "Manager", msg.c_str());
	}

	return value.get(name, "").asInt();
}

Session::Task Session::processJsonContent(std::string json)
{
	Json::Value root;
	Json::Reader reader;
	bool parsingSuccessful = reader.parse( json, root );

	if ( !parsingSuccessful )
	{
	    throw SandboxException(203, "Failed to parse configuration", reader.getFormattedErrorMessages().c_str());
	}

	Task task;
	task.id = getJsonInteger(root, JSON_ID_TASK);
	task.language = getJsonInteger(root, JSON_LANG);
	task.code = getJsonString(root, JSON_CODE);

	return task;
}

Session::Lang Session::generateMakefile(Task task)
{
	Session::Lang lang;
	std::string make_content;
	std::ofstream file(tmp_dir + "/" MAKEFILE);
	std::string query = "SELECT * FROM " DB_LANG_TABLE " WHERE " DB_FIELD_ID "=";

	query.append(std::to_string(task.language));
	query.append(" LIMIT 1");

	res = stmt->executeQuery(query);

	if (res->next()) {
		make_content = res->getString(DB_FIELD_MAKE);
		lang.id = res->getInt(DB_FIELD_ID);
		lang.mainfile = res->getString(DB_FIELD_MAIN_FILE_NAME);
		lang.mode = res->getInt(DB_FIELD_MODE);
	}

	if( !file.is_open() )
	{
		throw SandboxException(205, "Manager:", "makefile not open");
	}

	file << make_content;
	file.close();

	return lang;
}

int Session::generateFilesInput(Task task)
{
	std::string input_content;
	int idx = 0;

	std::string query = "SELECT * FROM " DB_INPUT_TABLE " WHERE " DB_FIELD_INPUT_TASKID "=";
	query.append(std::to_string(task.id));

	res = stmt->executeQuery(query);

	while (res->next()) {
		input_content = res->getString(DB_FIELD_INPUT_CONTENT);

		std::ofstream file(tmp_dir + "/" FILE_STDIN + std::to_string(idx++));
		if( !file.is_open() )
		{
			throw SandboxException(206, "Manager:", "file stdint not open");
		}
		file << input_content;
		file.close();
	}

	return idx;
}

bool Session::generateMainFile(Task task, Lang lang)
{
	std::ofstream file(tmp_dir + "/" + lang.mainfile);
	if( !file.is_open() )
	{
		throw SandboxException(207, "Manager:", "main file not open");
	}

	file << task.code;
	file.close();
	return true;
}

bool Session::generateConfigFile(Task task, Lang lang, int inputs)
{
	std::string limits;
	Json::Value root;
	root[JSON_MODE] = lang.mode;
	root[JSON_COUNT] = inputs;

	std::string query = "SELECT * FROM " DB_TASK_TABLE " WHERE " DB_FIELD_ID "=";

	query.append(std::to_string(task.id));
	query.append(" LIMIT 1");

	res = stmt->executeQuery(query);

	if (res->next()) {
		limits = res->getString(DB_FIELD_LIMITS);
		std::vector<std::string> strs;
		boost::split(strs, limits, boost::is_any_of(","));

		for (std::vector<std::string>::iterator it = strs.begin(); it != strs.end(); ++it)
		{
			root["limits"].append(std::stoi(*it));
		}

		Json::StyledWriter writer;
		std::string json = writer.write(root);
		std::ofstream file(tmp_dir + "/" + CONFIG_FILE);
		if( !file.is_open() )
		{
			throw SandboxException(207, "Manager:", "config file not open");
		}

		file << json;
		file.close();

		return true;
	}
	return false;
}

bool Session::prepareAndSendPackage(ClientSocket &sock, Lang lang, int n)
{
	std::string filename;
	std::vector<std::string> files;

	files.push_back(CONFIG_FILE);
	files.push_back(lang.mainfile);
	files.push_back(MAKEFILE);
	for(int i = 0; i < n; i++)
	{
		filename = std::to_string(i);
		filename = FILE_STDIN + filename;
		files.push_back(filename);
	}

	FileHelper *fh = new FileHelper(tmp_dir, "package.zip");
	fh->zip(files);
	fh->load();
	fh->send(sock);

	return true;
}

bool Session::captureResponse(ClientSocket &sock)
{
	FileHelper *fh = new FileHelper(tmp_dir, "response.zip");
	fh->recv(sock);
	fh->save();
	fh->unzip();

	return true;
}

int Session::logResult(ServerSocket & sock, Task task, Lang lang, int inputs)
{
	int id = -1;
	std::ifstream status_file(tmp_dir + "/" + STATUS_FILE);
	if( !status_file.is_open() )
	{
		throw SandboxException(208, "Status file not open");
	}

	Json::Value root;
	Json::Reader reader;
	if( !reader.parse(status_file, root) )
	{
		throw SandboxException(208, "Status file is wrong type");
	}

	char peer_addr_str[ INET_ADDRSTRLEN ];
	sockaddr_in addr = sock.getAddr();
	inet_ntop( AF_INET, &addr, peer_addr_str, INET_ADDRSTRLEN );
	std::string ip = peer_addr_str;

	Json::StyledWriter writer;
	std::string uuid = getJsonString(root, JSON_SESSION);
	int result = getJsonInteger(root, JSON_STATUS);
	std::string times = writer.write(root[JSON_TIME]);

	prep_stmt = con->prepareStatement("INSERT INTO `result` (`ip`, `sandbox_id`, `uuid`, `language_id`, `task_id`, `result`, `times`, `created_at`, `code`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
	prep_stmt->setString(1, ip);
	prep_stmt->setInt(2, id_sand);
	prep_stmt->setString(3, uuid);
	prep_stmt->setInt(4, lang.id);
	prep_stmt->setInt(5, task.id);
	prep_stmt->setInt(6, result);
	prep_stmt->setString(7, times);
	prep_stmt->setString(8, getTime());
	prep_stmt->setString(9, task.code);
	prep_stmt->execute();

	if( result == MSG_OK ) {
		std::string q = "SELECT LAST_INSERT_ID() as id;";
		res = stmt->executeQuery(q);
		if( res->next() )
		{
			id = res->getInt64(DB_FIELD_ID);

			if( inputs == 0 ) inputs = 1;
			for( int i = 0; i < inputs; i++ )
			{
				std::string filename( tmp_dir + "/" STDOUT_FILE );
				filename.append( std::to_string(i) );
				std::ifstream file( filename );
				std::string content, line;

				if( !file.is_open() )
				{
					throw SandboxException(209, "Output file not open");
				}

				while ( getline(file,line) )
				{
				  content.append(line + '\n');
				}

				size_t pos;
				size_t offset = 0;

				while ((pos = content.find("'", offset)) != std::string::npos)
				{
					content.replace(pos, 1, "\\'");
					offset = pos + 2;
				}

				content = content.substr(0, content.length() - 1);

				prep_stmt = con->prepareStatement("INSERT INTO `result_file` (`result_id`, `content`) VALUES (?, ?)");
				prep_stmt->setInt(1, id);
				prep_stmt->setString(2, content);
				prep_stmt->execute();
			}
		}
	}
	return id;
}

bool Session::prepareAndSendResponse(ServerSocket &sock, int result_id)
{
	std::ifstream status_file(tmp_dir + "/" + STATUS_FILE);
	if( !status_file.is_open() )
	{
		throw SandboxException(208, "Status file not open");
	}

	Json::Value root;
	Json::Reader reader;
	if( !reader.parse(status_file, root) )
	{
		throw SandboxException(208, "Status file is wrong type");
	}

	int status = getJsonInteger(root, JSON_STATUS);
	std::string uuid = getJsonString(root, JSON_SESSION);

	Json::StyledWriter writer;
	Json::Value root2;
	root2[JSON_STATUS] = status;
	root2[JSON_MESSAGE] = MESSAGES[status];
	root2[JSON_RESULT_ID] = result_id;
	std::string msg = writer.write(root2);

	sock
		<< "HTTP/1.1 200 OK\r\n"
		<< "Server: Sandbox V2\r\n"
		<< "Access-Control-Allow-Origin: *\r\n"
		<< "Content-Length: "
		<< std::to_string(msg.length())
		<< "\r\n"
		<< "Content-Type: application/json\r\n"
		<< "Connection: Closed\r\n\r\n"
		<< msg;

	return true;
}

std::string Session::getTime(){
	time_t rawtime;
	struct tm * timeinfo;
	char buffer [80];

	time (&rawtime);
	timeinfo = localtime (&rawtime);

	strftime (buffer,80,"%F %T",timeinfo);
	return std::string(buffer);
}
