/*
 * Sandbox.cpp
 *
 *  Created on: 25 lut 2016
 *      Author: lukasz
 */

#include <Sandbox.h>
#include <SandboxException.h>
#include <FileHelper.h>

#include <json/json.h>
#include <boost/timer.hpp>
#include <boost/filesystem.hpp>

#include <iostream>
#include <string>
#include <fstream>

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>

#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/resource.h>

Sandbox::Sandbox(char* id)
{
	this->id = id;
	work_dir = std::string(TMP_DIR) + std::string("/") + std::string(id);
	mkdir(work_dir.c_str(), 0755);
	clear();
}

Sandbox::~Sandbox()
{
	clear();
	std::string command = "rm -rf ";
	command.append(work_dir);
	system(command.c_str());
}

void Sandbox::clear()
{
	std::string command = "rm -rf ";
	command.append(work_dir + "/" STATUS_FILE " ");
	command.append(work_dir + "/" CONFIG_FILE " ");
	command.append(work_dir + "/" STDIN_FILE "* ");
	command.append(work_dir + "/" STDOUT_FILE "* ");
	command.append(work_dir + "/" MAKEFILE " ");
	command.append(work_dir + "/" FILE_RESULT " ");
	command.append(work_dir + "/" FILE_ERROR " ");
	command.append(work_dir + "/main* ");
	command.append(work_dir + "/*.zip");
	system(command.c_str());
}

void Sandbox::run_init(int idx){
	chdir(work_dir.c_str());

	std::string filein = STDIN_FILE;
	filein.append(std::to_string(idx));
	saved_stdin = dup(STDIN_FILENO);
	int fdstdin = open(filein.c_str(), O_RDWR | O_CREAT, 0666);
    dup2(fdstdin, STDIN_FILENO);
	close(fdstdin);

	std::string fileout = STDOUT_FILE;
	fileout.append(std::to_string(idx));
	saved_stdout = dup(STDOUT_FILENO);
	int fdstdout = open(fileout.c_str(), O_RDWR | O_CREAT, 0666);
    dup2(fdstdout, STDOUT_FILENO);
	close(fdstdout);

	saved_stderr = dup(STDERR_FILENO);
	int fdstderr = open(fileout.c_str(), O_RDWR | O_CREAT, 0666);
    dup2(fdstderr, STDERR_FILENO);
	close(fdstderr);
}

void Sandbox::run_deinit(){
	chdir("..");

	dup2(saved_stdin, STDIN_FILENO);
    dup2(saved_stdout, STDOUT_FILENO);
    dup2(saved_stderr, STDERR_FILENO);
}

void Sandbox::compile_init(){
	chdir(work_dir.c_str());
	remove(FILE_RESULT);
	remove(FILE_ERROR);

	saved_stdout = dup(STDOUT_FILENO);
	int fdstdout = open(FILE_RESULT, O_RDWR | O_CREAT, 0666);
    dup2(fdstdout, STDOUT_FILENO);
	close(fdstdout);

	saved_stderr = dup(STDERR_FILENO);
	int fdstderr = open(FILE_ERROR, O_RDWR | O_CREAT, 0666);
    dup2(fdstderr, STDERR_FILENO);
	close(fdstderr);
}

void Sandbox::compile_deinit(){
	chdir("..");

    dup2(saved_stdout, STDOUT_FILENO);
    dup2(saved_stderr, STDERR_FILENO);
}

bool Sandbox::processRequest(ServerSocket &sock)
{
	FileHelper *fh = new FileHelper(work_dir, "package.zip");
	fh->recv(sock);
	fh->save();
	fh->unzip();

	Json::Value json = loadConfig();
	Task task = processJsonConfig(json);
	int result = processing(task);
	generateStatusFile(result, time, task.count);
	sendResponse(sock, result, task.count);

	return true;
}

int Sandbox::getJsonInteger(Json::Value value, std::string name)
{
	if( !value.isMember(name) || !value.get(name, 0).isInt() )
	{
		std::string msg = name  + " in json file does not exist or is in the wrong format";
		throw SandboxException(102, "Sandbox", msg.c_str());
	}

	return value.get(name, "").asInt();
}

Json::Value Sandbox::loadConfig()
{
	Json::Value root;
	Json::Reader reader;
	std::ifstream file(work_dir + "/" CONFIG_FILE);
	if( !file.is_open() )
	{
		throw SandboxException(105, "Failed to open config file");
	}
	bool parsingSuccessful = reader.parse( file, root );

	if ( !parsingSuccessful )
	{
		throw SandboxException(101, "Failed to parse configuration", reader.getFormattedErrorMessages().c_str());
	}

	return root;
}

Sandbox::Task Sandbox::processJsonConfig(Json::Value value)
{
	Sandbox::Task task;
	task.count = getJsonInteger(value, JSON_COUNT);
	int mode = getJsonInteger(value, JSON_MODE);
	switch(mode)
	{
		case 0:
			task.mode = Sandbox::process_mode::MODE_COMPILE; break;
		case 1:
			task.mode = Sandbox::process_mode::MODE_INTERPRET; break;
		case 2:
			task.mode = Sandbox::process_mode::MODE_BAITCODE; break;
	}

	if( !value.isMember(JSON_LIMITS) || !value.get(JSON_LIMITS, 0).isArray() )
	{
		throw SandboxException(102, "Sandbox", JSON_LIMITS " in json file does not exist or is in the wrong format");
	}
	Json::Value limits = value.get(JSON_LIMITS, 0);

	for(unsigned int i = 0; i < limits.size(); i++)
	{
		task.limits[i] = limits[i].asInt();
	}

	time = new double[task.count];

	return task;
}

int Sandbox::processing(Task task)
{
	if( !boost::filesystem::exists(work_dir + "/" MAKEFILE) )
	{
		throw SandboxException(106, "Makefile not exist");
	}

	int result;
	if(task.mode == Sandbox::MODE_COMPILE || task.mode == Sandbox::MODE_BAITCODE)
	{
		compile_init();
		result = compile();
		compile_deinit();

		if( result )
		{
			return 1;
		}
	}
	for(int i = 0; i < task.count; i++)
	{
		run_init(i);
		result = run(i, task.limits);
		run_deinit();

		if( result == 1 )
		{
			return 2;
		}
		if( result == -1 )
		{
			throw SandboxException(107, "PrLimit", strerror(errno));
		}
	}
	return 0;
}

int Sandbox::compile(){
	int status, result = 0;
	int pid = fork();
	if ( pid == 0 )
	{
		exit( execl(MAKE, MAKE, "-s", "compile", NULL) );
	}
	else if( pid > 0 )
	{
		waitpid( pid, &status, 0 );

		if ( !WIFEXITED(status) || WEXITSTATUS(status) != 0 )
		{
			result = 1;
		}
	}

	return result;
}

int Sandbox::run(int idx, int limits[]){
	int status, result = 0, l_error = 0;
	int pid = fork();
	if ( pid == 0 )
	{
		exit( execl(MAKE, MAKE, "-s", "run", NULL) );
	}
	else if( pid > 0 )
	{
		bool l = setLimits(pid, limits);
		if( !l )
		{
			l_error = 1;
			kill(pid,SIGTERM);
		}

		int k_pid = startKillTimer(pid, GLOBAL_MAX_TIMME_EXEC);

		boost::timer t;
		waitpid( pid, &status, 0 );
		time[idx] = t.elapsed();

		if ( !WIFEXITED(status) || WEXITSTATUS(status) != 0 )
		{
			result = 1;
		}

		stopKillTimer(k_pid);

		if( l_error )
		{
			result = -1;
		}
	}
	return result;
}

bool Sandbox::generateStatusFile(int status, double time[], int n)
{
	Json::Value root;
	root[JSON_STATUS] = status;
	root[JSON_SESSION] = id;

	if( status == 0 )
	{
		for(int i = 0; i < n; i++)
		{
			root[JSON_TIME].append(time[i]);
		}
	}

	Json::StyledWriter writer;
	std::string json = writer.write(root);

	std::ofstream file(work_dir + "/" STATUS_FILE);
	if( !file.is_open() )
	{
		throw SandboxException(103, "Sandbox:", "status file can not create");
	}

	file << json;
	file.close();

	return true;
}

bool Sandbox::sendResponse(ServerSocket &sock, int status, int n)
{
	FileHelper *fh = new FileHelper(work_dir, "result.zip");
	std::vector<std::string> files;
	files.push_back(".status");

	if( status == 0 )
	{
		for(int i = 0; i < n; i++)
		{
			std::string filename = FILE_STDOUT;
			filename.append( std::to_string(i) );
			files.push_back( filename );
		}
	}

	fh->zip(files);
	fh->load();
	fh->send(sock);
	return true;
}

int Sandbox::startKillTimer(int pid, int time)
{
	int m_pid = fork();
	if(m_pid == 0)
	{
		sleep(time);
		kill(pid,SIGTERM);
		exit(0);
	}
	return m_pid;
}

void Sandbox::stopKillTimer(int k_pid)
{
	int status;
	kill(k_pid, SIGKILL);
	waitpid( k_pid, &status, 0 );
}

bool Sandbox::setLimits(int pid, int all_limits[])
{
	for(int i = 0; i < __rlimit_resource::RLIMIT_NLIMITS; i++)
	{
	    if( all_limits[i] != LIMIT_DONT_CAHNGE )
	    {
		    struct rlimit limit;
			__rlimit_resource i_limit = (__rlimit_resource) i;
			limit.rlim_cur = limit.rlim_max = all_limits[i_limit];

			if( prlimit(pid, i_limit, &limit, NULL) == -1 )
			{
				return false;
			}

	    }
	}

	return true;
}
