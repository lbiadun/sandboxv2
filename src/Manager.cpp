/*
 * Manager.cpp
 *
 *  Created on: 19 mar 2016
 *      Author: lukasz
 */

#include <Manager.h>
#include <Session.h>
#include <SandboxException.h>
#include <Log.h>

#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/resource.h>

#include <uuid/uuid.h>
#include <json/json.h>
#include <bsd/libutil.h>

Manager::Manager()
{ }

Manager::~Manager()
{
	deinit();
}

void Manager::init()
{
	std::cout << "Loading configuration... ";
	loadConfig();
	std::cout << "Done\nConnect to database... ";
	connectingDatabase();
	std::cout << "Done\nLoading sandboxes configuration... ";
	loadSandboxConfig();
	std::cout << "Done\n";
}

int Manager::start()
{
	struct pidfh *pfh;
	pid_t otherpid;
	system("mkdir -p " PID_DIR);
	system("mkdir -p " LOG_DIR);
	pfh = pidfile_open(PID_FILE, 0600, &otherpid);
	if (pfh == NULL)
	{
	    if (errno == EEXIST)
	    {
	    	std::cout << "SandboxManager already running, pid: " << (intmax_t)otherpid << std::endl;
	   		return 1;
	    }
	    std::cout << "Cannot open or create pidfile" << std::endl;
	    return 1;
	}

	init();
	std::cout << "Launch instance sandbox...\n";
	for(int i = 0; i < sandbox_count; i++)
	{
		runSandbox(sandboxes[i]);
	}

	std::cout << "SandboxManager starting [port " << port << "] ...\n";

	sleep(1);

	if (daemon(0, 0) == -1)
	{
		std::cout << "Cannot daemonize" << std::endl;
	    pidfile_remove(pfh);
	    exit(EXIT_FAILURE);
	}
	pidfile_write(pfh);

	run();
	return 0;
}

int Manager::stop()
{
	struct pidfh *pfh;
	pid_t otherpid;

	pfh = pidfile_open(PID_FILE, 0600, &otherpid);
	if (pfh == NULL) {
		init();
		std::cout << "Exit instance sandbox...\n";
		for(int i = 0; i < sandbox_count; i++)
		{
			exitSandbox(sandboxes[i]);
		}

		std::cout << "Exit sandbox-manager...\n";
		system("kill -9 $(cat " PID_FILE ")");
		pidfile_remove(pfh);
		return 0;
	} else {
		std::cout << "Sandbox-manager not running\n";
	}

	return 1;
}

int Manager::restart()
{
	int res = 0;
	res = stop();
	if(res) return res;
	res = start();
	return res;
}

void Manager::run()
{
	try
	{
		ServerSocket server(port);

		while ( true )
		{
			ServerSocket new_sock;
			server.accept ( new_sock );

			try
			{
				processRequest(new_sock);
			}
			catch ( SandboxException& e )
			{
				//e.printError();
				Log::log(e.getMsg());
			}
			catch ( sql::SQLException &e)
			{
				//s.generateStatusFile( 3 );
				//s.sendResponse(new_sock, 0);

				Log::log(
						std::string("# ERR: ")
						+ e.what()
						+ std::string(" (MySQL error code: ")
						+ std::to_string(e.getErrorCode())
						+ std::string(", SQLState: ")
						+ e.getSQLState() + " )");
			}
		}
	} catch ( SandboxException& e ) {
		//e.printError();
		Log::log(e.getMsg());
	}
}

int Manager::status()
{
	bool run = true;

	struct pidfh *pfh;
	pid_t otherpid;
	pfh = pidfile_open(PID_FILE, 0600, &otherpid);
	if (pfh == NULL)
	{
		if (errno == EEXIST)
		{
			run = false;
		}
	}


	std::cout << "Sandbox-Manager run: " << (!run ? "yes" : "no")
			  << "\n--------------------------------------\n";

	init();
	std::cout << "*\tPID\tPORT\tRUN\tACTIVE\n";
	for(int i = 0; i < sandbox_count; i++)
	{
		std::cout << i << "\t";
		std::cout << sandboxes[i].pid << "\t";
		std::cout << sandboxes[i].port << "\t";
		std::cout << sandboxes[i].run << "\t";
		std::cout << sandboxes[i].active << "\n";
	}

	return 0;
}

int Manager::provision()
{
	std::cout << "Clear old environment... " << std::flush;
	if( system("sudo rm -rf " CHROOT_SANDBOX "/*") ) 				return EXIT_FAILURE;
	std::cout << "Done" << std::endl;

	std::cout << "Prepare new environment - wily... " << std::flush;
	if( system("sudo cp -rf " CHROOT_WILY "/* " CHROOT_SANDBOX) ) 	return EXIT_FAILURE;
	std::cout << "Done" << std::endl;

	int status;
	int pid = fork();
	if ( pid == 0 )
	{
		if( chroot(CHROOT_SANDBOX) )								exit( EXIT_FAILURE );

		std::cout << "Create sandbox user... " << std::flush;
		if( system("useradd sandbox") ) 							exit( EXIT_FAILURE );
		if( system("mkdir /home/sandbox") ) 						exit( EXIT_FAILURE );
		if( system("chown sandbox:sandbox /home/sandbox") ) 		exit( EXIT_FAILURE );
		if( system("chmod 777 /tmp") ) 								exit( EXIT_FAILURE );
		if( system("mount -t proc proc /proc") ) 					exit( EXIT_FAILURE );

		exit( EXIT_SUCCESS );
	}
	else if( pid > 0 )
	{
		waitpid( pid, &status, 0 );
	}
	std::cout << "Done" << std::endl;

	if( status == EXIT_SUCCESS )
	{
		return installLang();
	}
	else
	{
		return EXIT_FAILURE;
	}
}

int Manager::installLang()
{
	std::cout << "Installing the required languages... " << std::endl;
	init();

	std::string query = "SELECT * FROM " DB_LANG_TABLE;
	res = stmt->executeQuery(query);

	while (res->next()) {
		int status;
		int pid = fork();
		if ( pid == 0 )
		{
			std::cout << "Install: " << res->getString( DB_FIELD_NAME ) << std::endl;

			if( chroot(CHROOT_SANDBOX) )	exit( EXIT_FAILURE );

			std::string command( "apt-get -y install " );
			command.append( res->getString( DB_FIELD_INSTALL ) );

			if( system(command.c_str()) )	exit( EXIT_FAILURE );

			exit( EXIT_SUCCESS );
		}
		else if( pid > 0 )
		{
			waitpid( pid, &status, 0 );
		}
	}
	std::cout << "Installing the required languages... complete" << std::endl;

	return EXIT_SUCCESS;
}

int Manager::enterChroot(){
	int status;
	int pid = fork();
	if ( pid == 0 )
	{
		if( system("sudo chroot " CHROOT_SANDBOX) ) 				exit( EXIT_FAILURE );
	}
	else if( pid > 0 )
	{
		waitpid( pid, &status, 0 );
	}

	return EXIT_SUCCESS;
}

void Manager::deinit()
{
	delete res;
	delete stmt;
	delete con;
}

std::string Manager::getJsonString(Json::Value value, std::string name)
{
	if( !value.isMember(name) || !value.get(name, "").isString() )
	{
		std::string msg = name  + " in json file does not exist or is in the wrong format";
		throw SandboxException(204, "Manager", msg.c_str());
	}

	return value.get(name, "").asString();
}

void Manager::loadConfig()
{
	std::ifstream file(GLOBAL_CONFIG_FILE);
	if( !file.is_open() )
	{
		throw SandboxException(301, "Config file not open");
	}

	Json::Value root;
	Json::Reader reader;

	if( !reader.parse(file, root) || !root.isMember(JSON_DB) )
	{
		throw SandboxException(302, "Config file parse error");
	}

	port = root.get(JSON_PORT, DEFAUL_PORT).asInt();
	db_conn.name = root[JSON_DB].get(JSON_NAME, DB_NAME).asString();
	db_conn.pass = root[JSON_DB].get(JSON_PASS, DB_PASS).asString();
	db_conn.user = root[JSON_DB].get(JSON_USER, DB_USER).asString();
	db_conn.host = root[JSON_DB].get(JSON_HOST, DB_HOST).asString();
}

void Manager::connectingDatabase()
{
	driver = sql::mysql::get_mysql_driver_instance();
	con = driver->connect(std::string("tcp://") + db_conn.host + ":3306", db_conn.user, db_conn.pass);

	if( !con->isValid() )
	{
		throw SandboxException(201, "Manager:", "database not open");
	}

	stmt = con->createStatement();
	stmt->execute(std::string("USE ") + db_conn.name);
	res = NULL;
}

void Manager::loadSandboxConfig()
{
	std::string query = "SELECT * FROM " DB_SANDBOX_TABLE;
	res = stmt->executeQuery(query);
	sandbox_count = res->rowsCount();
	sandboxes = new SandboxCopy[sandbox_count];

	int idx = 0;

	while (res->next()) {
		sandboxes[idx].active = res->getBoolean(DB_FIELD_ACTIVE);
		sandboxes[idx].run = res->getBoolean(DB_FIELD_RUN);
		sandboxes[idx].ip = res->getString(DB_FIELD_IP);
		sandboxes[idx].port = res->getInt(DB_FIELD_PORT);
		sandboxes[idx].uuid = res->getInt(DB_FIELD_ID);
		sandboxes[idx].pid = res->getInt(DB_FIELD_PID);

		idx++;
	}

}

bool Manager::runSandbox(SandboxCopy &sand)
{
	int pid = fork();
	if ( pid == 0 )
	{
		exit(execl(SANDBOX_EXE, SANDBOX_EXE, std::to_string(sand.port).c_str(), NULL));
	}
	else if( pid > 0 )
	{
		sand.pid = pid;
		sand.run = true;
		sand.active = false;
	}
	else
	{
		return false;
	}
	sandbox_count_curr++;

	notifyStatusSandBox(sand);
	return true;
}

bool Manager::exitSandbox(SandboxCopy & sand)
{
	int status;
	kill(sand.pid, SIGKILL);
	waitpid( sand.pid, &status, 0 );
	sand.run = false;
	sand.active = false;
	sandbox_count_curr--;

	notifyStatusSandBox(sand);
	return true;
}

bool Manager::notifyStatusSandBox(SandboxCopy &sand)
{
	std::string query = "UPDATE " DB_SANDBOX_TABLE " SET " DB_FIELD_RUN "=";
	query.append(std::to_string(sand.run));
	query.append(", " DB_FIELD_ACTIVE "=");
	query.append(std::to_string(sand.active));
	query.append(", " DB_FIELD_PID "=");
	query.append(std::to_string(sand.pid));
	query.append(" WHERE " DB_FIELD_ID "=");
	query.append(std::to_string(sand.uuid));

	int affected = stmt->executeUpdate(query);

	if( affected > 0 )
	{
		return true;
	}
	else
	{
		return false;
	}
}

void Manager::processRequest(ServerSocket &sock)
{
	SandboxCopy sand = getFreeSandbox();
	sand.active = true;
	notifyStatusSandBox(sand);

	Session *sm = new Session(sand, db_conn);
	try {
		sm->processRequest(sock);
	} catch (SandboxException& e) {

	}

	sand.active = false;
	notifyStatusSandBox(sand);

	delete sm;
}

Manager::SandboxCopy Manager::getFreeSandbox(){
	int id = -1;
	std::string query = "SELECT * FROM " DB_SANDBOX_TABLE " WHERE " DB_FIELD_ACTIVE " = 0";

	while(id < 0)
	{
		res = stmt->executeQuery(query);
		if (res->next()) {
			id = res->getInt(DB_FIELD_ID);
		}

		usleep(100);
	}

	for(int i = 0; i < sandbox_count; i++)
	{
		if(id == sandboxes[i].uuid)
		{
			return sandboxes[i];
		}
	}

	throw SandboxException(220, "Sandbox not found");
}
