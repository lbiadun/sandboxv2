/*
 * FileHelper.cpp
 *
 *  Created on: 2 mar 2016
 *      Author: lukasz
 */

#include <FileHelper.h>
#include <SandboxException.h>
#include <Config.h>
#include <zip.h>

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <dirent.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/resource.h>

#include <iostream>
#include <fstream>
#include <vector>

FileHelper::FileHelper( std::string d, std::string n )
{
	dir = d;
	name = n;
	isValid = false;
	memblock = new char[MAX_FILE_SIZE];
}

FileHelper::~FileHelper()
{
}

void FileHelper::load()
{
	std::ifstream file;
	std::string filename = dir + "/" + name;
	file.open( filename.c_str(), std::ios::in|std::ios::binary|std::ios::ate );

	if ( file.is_open() )
	{
		size = file.tellg();
		memblock = new char [size];
		file.seekg(0, std::ios::beg);
		file.read(memblock, size);
		file.close();

		isValid = true;
	}
	else
	{
		throw SandboxException(13, "File not exist");
	}

}

void FileHelper::save()
{
	if( !isValid )
	{
		throw SandboxException(13, "File is not loaded");
	}

	std::ofstream file;
	std::string filename = dir + "/" + name;
	file.open(filename.c_str(), std::ios::out|std::ios::binary);

	if(file.is_open()){
		file.write(memblock, size);
		file.close();
	}
	else
	{
		throw SandboxException(13, "Problem with opening a file");
	}
}

void FileHelper::send(Socket &sock)
{
	if( !isValid )
	{
		throw SandboxException(13, "File is not loaded");
	}
	if( ! sock.send(memblock, size) )
	{
		throw SandboxException (11, "Could not write to socket.");
	}
}

void FileHelper::recv(Socket &sock)
{
	memblock = new char[MAXRECV + 1];

	int status = sock.recv(memblock);
	if ( ! status )
	{
		throw SandboxException (11, "Could not read from socket.");
	}
	size = status;
	isValid = true;
}

void FileHelper::safe_create_dir(const char *dir)
{
    if (mkdir(dir, 0755) < 0) {
        if (errno != EEXIST) {
            perror(dir);
            exit(1);
        }
    }
};

void FileHelper::zip(std::vector<std::string> files)
{
	struct zip *za;
	char buf[100];
	zip_source_t *s;
	int err;
	std::string path, filename;

	path = dir + "/" + name;
	if ( (za = zip_open(path.c_str(), ZIP_CREATE, &err)) == NULL )
	{
		zip_error_to_str(buf, sizeof(buf), err, errno);
		throw SandboxException(12, "Can't create zip archive", name.c_str(), buf);
	}

	for(std::vector<std::string>::iterator it = files.begin(); it != files.end(); ++it)
	{
		filename = *it;
		path = dir + "/" + filename;
		if ( ( s=zip_source_file(za, path.c_str(), 0, -1)) == NULL ||
			   zip_file_add(za, filename.c_str(), s, ZIP_FL_ENC_UTF_8) < 0 )
		{
			zip_source_free(s);
			throw SandboxException(12, "Error adding result file to archive", zip_strerror(za));
		}
	}

	if (zip_close(za) == -1)
	{
		throw SandboxException(12, "Can't close zip archive");
	}
}

void FileHelper::unzip(){
	struct zip *za;
	struct zip_file *zf;
	struct zip_stat sb;
	char buf[100];
	int err;
	int i, len;
	int fd;
	long long sum;
	//char dest_name[255];
	std::string dest;
	std::string filename = dir + "/" + name;

	if ((za = zip_open(filename.c_str(), 0, &err)) == NULL)
	{
		zip_error_to_str(buf, sizeof(buf), err, errno);
		throw SandboxException(12, "Can't open zip archive", name.c_str(), buf);
	}

	for (i = 0; i < zip_get_num_entries(za, 0); i++)
	{
		if (zip_stat_index(za, i, 0, &sb) == 0)
		{
			len = strlen(sb.name);
			if (sb.name[len - 1] == '/')
			{
				safe_create_dir(sb.name);
			}
			else
			{
				zf = zip_fopen_index(za, i, 0);
				if (!zf)
				{
					throw SandboxException(12, "Boese, boese");
				}

				//strcpy(dest_name, dest);
				//strcat(dest_name, sb.name);

				dest = dir + "/" + sb.name;
				fd = open(dest.c_str(), O_RDWR | O_TRUNC | O_CREAT, 0644);
				if (fd < 0)
				{
					throw SandboxException(12, "Boese, boese");
				}

				sum = 0;
				while (sum != sb.size)
				{
					len = zip_fread(zf, buf, 100);
					if (len < 0)
					{
						throw SandboxException(12, "Boese, boese");
					}
					write(fd, buf, len);
					sum += len;
				}
				close(fd);
				zip_fclose(zf);
			}
		}
		else
		{
			printf("File[%s] Line[%d]/n", __FILE__, __LINE__);
		}
	}

	if (zip_close(za) == -1) {
		throw SandboxException(12, "Can't close zip archive", name.c_str());
	}
}
