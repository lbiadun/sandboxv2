// Implementation of the ServerSocket class

#include "ServerSocket.h"
#include "SandboxException.h"

#include <iostream>

ServerSocket::ServerSocket(int port) {
	if (!Socket::create()) {
		throw SandboxException(10, "Could not create server socket.");
	}

	if (!Socket::bind(port)) {
		throw SandboxException(10, "Could not bind to port.");
	}

	if (!Socket::listen()) {
		throw SandboxException(10, "Could not listen to socket.");
	}

}

ServerSocket::~ServerSocket() {
}


const ServerSocket& ServerSocket::operator << ( const std::string& s ) const
{
	if ( ! Socket::send ( s ) )
    {
		throw SandboxException (10, "Could not write to socket." );
    }

	return *this;
}


const ServerSocket& ServerSocket::operator >> ( std::string& s ) const
{
	if ( ! Socket::recv ( s ) )
    {
		throw SandboxException (10, "Could not read from socket." );
    }

	return *this;
}

void ServerSocket::accept(ServerSocket& sock) {
	if (!Socket::accept(sock)) {
		throw SandboxException(10, "Could not accept socket.");
	}
}
