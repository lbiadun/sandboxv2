/*
 * Log.cpp
 *
 *  Created on: 25 mar 2016
 *      Author: lukasz
 */

#include <Log.h>
#include <iostream>
#include <fstream>
#include <ctime>
#include <cstdio>

#include <Config.h>

Log::Log() { }

Log::~Log() { }

void Log::log(std::string msg)
{
	std::ofstream file(LOG_FILE, std::ios::app);
    time_t t;
    time( & t );
    std::string date_time = asctime( localtime( &t ) );
    date_time = date_time.substr(0,24);
	if( file.is_open() )
	{
		file << "[" << date_time << "] " << msg << std::endl;
	}
	file.close();
}
