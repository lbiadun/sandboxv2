// Implementation of the ClientSocket class

#include "ClientSocket.h"
#include <SandboxException.h>

#include <iostream>

ClientSocket::ClientSocket(std::string host, int port) {
	if (!Socket::create()) {
		throw SandboxException(10, "Could not create client socket.");
	}

	if (!Socket::connect(host, port)) {
		throw SandboxException(10, "Could not bind to port.");
	}

}

const ClientSocket& ClientSocket::operator << ( const std::string& s ) const
{
	if ( ! Socket::send ( s ) )
    {
      throw SandboxException (10, "Could not write to socket." );
    }

	return *this;
}


const ClientSocket& ClientSocket::operator >> ( std::string& s ) const
{
	if ( ! Socket::recv ( s ) )
    {
      throw SandboxException (10, "Could not read from socket." );
    }

	return *this;
}
