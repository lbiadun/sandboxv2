function notifyLimits(){
	var limits_arr = [];

	for(var i = 0; i < 16; i++) {
		var limit = $("#limit-" + i).val();
		limits_arr[i] = limit;
	}

	var limits = limits_arr.join(",");
	$("#task_limits").val(limits);
}

function loadLimits(){
	var limits = $("#task_limits").val();
	var limits_arr = limits.split(",");

	for(var i = 0; i < 16; i++) {
		$("#limit-" + i).val( limits_arr[i] );
	}
}


$( document ).ready(function(){
	$("#submit_btn").click(function(){
		$("[name='task']").submit();
	});

	loadLimits();

	for(var i = 0; i < 16; i++) {
		$("#limit-" + i).change(function(){
			notifyLimits();
		});
	}
});