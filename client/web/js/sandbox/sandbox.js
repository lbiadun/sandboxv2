var judgeRun = function(resultId){
	$("#loaderContent").html("Rating result");
	$('#loaderModal').modal('show');
	$.ajax({
		url: "http://localhost:8000/judge/rating/" + resultId,
		method: "POST",
		dataType: "json"
	})
	.done(function( data ){
		$('#infoContent').html("<h3>Finish!</h3>");
		$('#infoModal').modal('show');
	})
	.fail(function( jqXHR, textStatus ) {
		$('#infoContent').html("Request failed: " + textStatus);
		$('#infoModal').modal('show');
	})
	.always(function(){
		$('#loaderModal').modal('hide');
	});
}

var sandboxRun = function(event){
	task = {
		task_id :  parseInt(event.data.taskSelect.val()),
		language : parseInt(event.data.langSelect.val()),
		code : event.data.editor.getValue()
	};
	$("#loaderContent").html("Compiling and runing...");
	$('#loaderModal').modal('show');

	$.ajax({ 
		url: "http://localhost:8080",
		contentType: "text/plain",
		crossDomain: true,
		dataType: "json",
		processData: false,
		data: JSON.stringify(task),
		method: "POST"
	})
	.done(function( data ){
		if(data.status == 0){
			judgeRun( data.result_id );
		} else {
			$('#infoContent').html("<h3>Error</h3><p>" + data.msg + "</p>");
			$('#infoModal').modal('show');
		}
	})
	.fail(function( jqXHR, textStatus ) {
		$('#infoContent').html("Request failed: " + textStatus);
		$('#infoModal').modal('show');
	})
	.always(function(){
		$('#loaderModal').modal('hide');
	});
}