$( document ).ready(function(){
    var editor = ace.edit("editor");
    editor.setTheme("ace/theme/chrome");
    editor.getSession().setMode("ace/mode/c_cpp");

    $("#submit_btn").click({
    	editor: editor,
    	taskSelect: $( "#run_task" ),
    	langSelect: $( "#run_language" )
    }, sandboxRun);

	$("#run_language").change(function(){
		switch( $("#run_language option:selected").text() ){
			case "GCC":
    			editor.getSession().setMode("ace/mode/c_cpp");
				break;
            case "PHP":
                editor.getSession().setMode("ace/mode/php");
                break;
            case "C#":
                editor.getSession().setMode("ace/mode/csharp");
                break;
            case "C++":
                editor.getSession().setMode("ace/mode/c_cpp");
                break;
            case "Java":
                editor.getSession().setMode("ace/mode/java");
                break;
            case "Python":
                editor.getSession().setMode("ace/mode/python");
                break;
		}
	});
});