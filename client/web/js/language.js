function notifyMakefile(){
	var compile = $("#compile").val();
	var run = $("#run").val();
	var makefile = "compile:\n\t" + compile + "\n\nrun:\n\t" + run;
	$("#language_makefile").val(makefile);
}

function loadMakefile(){
	var makefile = $("#language_makefile").val();
	var run = makefile.search("\n\nrun:\n\t");

	var commandCompile = makefile.substr(10, run - 10);
	var commandRun = makefile.substr(run + 8, makefile.length - run - 8);

	$("#compile").val(commandCompile);
	$("#run").val(commandRun);
}

$( document ).ready(function(){
	$("#submit_btn").click(function(){
		$("[name='language']").submit();
	});

	loadMakefile();

	$("#compile").change(function(){
		notifyMakefile();
	});

	$("#run").change(function(){
		notifyMakefile();
	});
});