<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use AppBundle\Form\Type\RunType;

/**
 * Run controller.
 *
 */
class RunController extends Controller
{
    /**
     * @Route("/", name="run_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $run = array();
        $form = $this->createForm(new RunType(), $run);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //$em = $this->getDoctrine()->getManager();
            //$em->persist($language);
            //$em->flush();

            //return $this->redirectToRoute('management_index', array('id' => $language->getId()));
        }

        return $this->render('run/index.html.twig', array(
            'form' => $form->createView(),
        ));
    }

}

