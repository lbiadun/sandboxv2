<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Judge;
use AppBundle\Entity\Result;
use AppBundle\Form\Type\JudgeType;
use Symfony\Component\HttpFoundation\JsonResponse;
/**
 * Task controller.
 *
 * @Route("/judge")
 */
class JudgeController extends Controller
{

    /**
     * @Route("/rating/{id}", name="judge_rating")
     * @Method({"GET", "POST"})
     */
    public function ratingAction(Request $request, Result $result)
    {
        $em = $this->getDoctrine()->getManager();
        $judgeClass = 'AppBundle\\Judge\\' . $result->getTask()->getJudge()->getAdapter();
        $judge = new  $judgeClass($em);
        $judge->process($result);
        $rate = $judge->getRate($result);

        return new JsonResponse(array('rate' => $rate));
    }

    /**
     * @Route("/new", name="judge_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $judge = new Judge();
        $form = $this->createForm(new JudgeType(), $judge);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($judge);
            $em->flush();

            return $this->redirectToRoute('management_index', array('id' => $judge->getId()));
        }

        return $this->render('judge/create.html.twig', array(
            'judge' => $judge,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/{id}/edit", name="judge_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Judge $judge)
    {
        $deleteForm = $this->createDeleteForm($judge);
        $editForm = $this->createForm(new  JudgeType(), $judge);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($judge);
            $em->flush();

            return $this->redirectToRoute('management_index', array('id' => $judge->getId()));
        }
        
        return $this->render('judge/details.html.twig', array(
            'judge' => $judge,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Route("/{id}", name="judge_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Judge $judge)
    {
        $form = $this->createDeleteForm($judge);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($judge);
            $em->flush();
        }

        return $this->redirectToRoute('management_index');
    }

    /**
     * @param Task $task The Task entity
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Judge $judge)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('judge_delete', array('id' => $judge->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
