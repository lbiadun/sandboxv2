<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Result;
use AppBundle\Entity\ResultFile;

use AppBundle\Judge\JudgeAdapterInterface;
use AppBundle\Judge\MainJudge;

/**
 * Task controller.
 *
 * @Route("/result")
 */
class ResultController extends Controller
{

    public $status = array(
        -1=> "Unknow",
        0 => "All correct",
        1 => "Compile error",
        2 => "Run error"
    );

    /**
     * @Route("/", name="result_index")
     * @Method({"GET"})
     */
    public function indexAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $results = $em->getRepository('AppBundle:Result')->findBy(array(), array('createdAt' => 'DESC'));

        foreach ($results as $result) {
            $judgeClass = 'AppBundle\\Judge\\' . $result->getTask()->getJudge()->getAdapter();
            $judge = new  $judgeClass($em);
            $result->setRate($judge->getRate($result));
        }

        return $this->render('result/index.html.twig', array(
            'results' => $results,
            'status' => $this->status
        ));
    }


    /**
     * @Route("/{id}", name="result_show")
     * @Method({"GET"})
     */
    public function showAction(Request $request, Result $result){
        $em = $this->getDoctrine()->getManager();
        $judgeClass = 'AppBundle\\Judge\\' . $result->getTask()->getJudge()->getAdapter();
        $judge = new  $judgeClass($em);
        $result->setRate($judge->getRate($result));

        return $this->render('result/details.html.twig', array(
            'result' => $result,
            'status' => $this->status,
            'times' => json_decode($result->getTimes())
        ));
    }

    /**
     * @Route("/compare/{id}", name="result_compare")
     * @Method({"GET"})
     */
    public function compareAction(Request $request, ResultFile $resultFile){
        $outputFiles = $resultFile->getResult()->getTask()->getOutputFiles()->getValues();
        $resultFiles = $resultFile->getResult()->getResultFiles()->getValues();

        $outputFile = null;
        for($i = 0; $i < count($resultFiles); $i++) {
            if($resultFiles[$i]->getId() == $resultFile->getId()){
                $outputFile = $outputFiles[$i];
            }
        }

        return $this->render('result/compare.html.twig', array(
            'resultFile' => $resultFile,
            'outputFile' => $outputFile
        ));
    }
}