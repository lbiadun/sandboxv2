<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\InputFile;
use AppBundle\Entity\OutputFile;
use AppBundle\Form\Type\StdType;

/**
 * Task controller.
 *
 * @Route("/std")
 */
class StdController extends Controller
{

    /**
     * @Route("/{taskId}/new", name="std_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, $taskId)
    {
        $em = $this->getDoctrine()->getManager();
        $task = $em->getRepository('AppBundle:Task')->findOneById($taskId);
        $inputFile = new InputFile();
        $inputFile->setTask($task);
        $outputFile = new OutputFile();
        $outputFile->setTask($task);

        $form = $this->createForm(new StdType(), array(
            "contentIn" => '',
            "contentOut" => ''
        ));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $inputFile->setContent($form->getData()["contentIn"]);
            $outputFile->setContent($form->getData()["contentOut"]);

            $em = $this->getDoctrine()->getManager();
            $em->persist($inputFile);
            $em->persist($outputFile);
            $em->flush();

            return $this->redirectToRoute('task_edit', array('id' => $taskId));
        }

        return $this->render('std/create.html.twig', array(
            'taskId' => $taskId,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/{idIn}/{idOut}/edit", name="std_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, $idIn, $idOut)
    {
        $em = $this->getDoctrine()->getManager();
        $inputFile = $em->getRepository('AppBundle:InputFile')->findOneById($idIn);
        $outputFile = $em->getRepository('AppBundle:OutputFile')->findOneById($idOut);
        $taskId = $inputFile->getTask()->getId();

        $std = array(
            "contentIn" => $inputFile->getContent(),
            "contentOut" => $outputFile->getContent()
        );

        $deleteForm = $this->createDeleteForm($inputFile, $outputFile);
        $editForm = $this->createForm(new  StdType(), $std);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $inputFile->setContent($editForm->getData()["contentIn"]);
            $outputFile->setContent($editForm->getData()["contentOut"]);

            $em = $this->getDoctrine()->getManager();
            $em->persist($inputFile);
            $em->persist($outputFile);
            $em->flush();

            return $this->redirectToRoute('task_edit', array('id' => $taskId));
        }
        
        return $this->render('std/details.html.twig', array(
            'inputFile' => $inputFile,
            'outputFile' => $outputFile,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'taskId' => $taskId
        ));
    }

    /**
     * @Route("/{idIn}/{idOut}", name="std_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $idIn, $idOut)
    {
        $em = $this->getDoctrine()->getManager();
        $inputFile = $em->getRepository('AppBundle:InputFile')->findOneById($idIn);
        $outputFile = $em->getRepository('AppBundle:OutputFile')->findOneById($idOut);

        $form = $this->createDeleteForm($inputFile, $outputFile);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($inputFile);
            $em->remove($outputFile);
            $em->flush();
        }

        $taskId = $inputFile->getTask()->getId();
        return $this->redirectToRoute('task_edit', array('id' => $taskId));
    }

    /**
     * @param Task $task The Task entity
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(InputFile $inputFile, OutputFile $outputFile)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('std_delete', array(
                'idIn' => $inputFile->getId(),
                'idOut' => $outputFile->getId()
            )))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
