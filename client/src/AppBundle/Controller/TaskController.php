<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Task;
use AppBundle\Form\Type\TaskType;

/**
 * Task controller.
 *
 * @Route("/task")
 */
class TaskController extends Controller
{

    /**
     * @Route("/new", name="task_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $task = new Task();
        $task->setLimits("-1,-1,-1,65536,0,-1,7866,1024,65536,-1,-1,7866,6536,0,0,-1");
        $form = $this->createForm(new TaskType(), $task);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($task);
            $em->flush();

            return $this->redirectToRoute('task_edit', array('id' => $task->getId()));
        }

        return $this->render('task/create.html.twig', array(
            'task' => $task,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/{id}/edit", name="task_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Task $task)
    {
        $deleteForm = $this->createDeleteForm($task);
        $editForm = $this->createForm(new TaskType(), $task);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($task);
            $em->flush();

            return $this->redirectToRoute('management_index', array('id' => $task->getId()));
        }

        $limits = array(
            array(
                'name' => 'CPU',
                'field' => 'limit-0',
                'description' => 'Per-process CPU limit, in seconds.'
            ),
            array(
                'name' => 'FSIZE',
                'field' => 'limit-1',
                'description' => 'Largest file that can be created, in bytes.'
            ),
            array(
                'name' => 'DATA',
                'field' => 'limit-2',
                'description' => 'Maximum size of data segment, in bytes.'
            ),
            array(
                'name' => 'STACK',
                'field' => 'limit-3',
                'description' => 'Maximum size of stack segment, in bytes.'
            ),
            array(
                'name' => 'CORE',
                'field' => 'limit-4',
                'description' => 'Largest core file that can be created, in bytes.'
            ),
            array(
                'name' => 'RSS',
                'field' => 'limit-5',
                'description' => 'Largest resident set size, in bytes. This affects swapping; processes that are exceeding their resident set size will be more likely to have physical memory taken from them.'
            ),
            array(
                'name' => 'NPROC',
                'field' => 'limit-6',
                'description' => 'Number of processes.'
            ),
            array(
                'name' => 'NOFILE',
                'field' => 'limit-7',
                'description' => 'Number of open files.'
            ),
            array(
                'name' => 'MEMLOCK',
                'field' => 'limit-8',
                'description' => 'Locked-in-memory address space.'
            ),
            array(
                'name' => 'AS',
                'field' => 'limit-9',
                'description' => 'Address space limit.'
            ),
            array(
                'name' => 'LOCKS',
                'field' => 'limit-10',
                'description' => 'Maximum number of file locks.'
            ),
            array(
                'name' => 'SIGPENDING',
                'field' => 'limit-11',
                'description' => 'Maximum number of pending signals.'
            ),
            array(
                'name' => 'MSGQUEUE',
                'field' => 'limit-12',
                'description' => 'Maximum bytes in POSIX message queues.'
            ),
            array(
                'name' => 'NICE',
                'field' => 'limit-13',
                'description' => 'Maximum nice priority allowed to raise to. Nice levels 19 .. -20 correspond to 0 .. 39 values of this resource limit.'
            ),
            array(
                'name' => 'RTPRIO',
                'field' => 'limit-14',
                'description' => 'Maximum realtime priority allowed for non-priviledged processes.'
            ),
            array(
                'name' => 'RTTIME',
                'field' => 'limit-15',
                'description' => 'Maximum CPU time in µs that a process scheduled under a real-time scheduling policy may consume without making a blocking system call before being forcibly descheduled.'
            ),
        );

        $em = $this->getDoctrine()->getManager();
        $fileInput = $em->getRepository('AppBundle:InputFile')->findByTask($task);
        $fileOutput = $em->getRepository('AppBundle:OutputFile')->findByTask($task);

        $files = array();
        for($i = 0; $i < count($fileInput); $i++) {
            $f = array();
            $f['in'] = $fileInput[$i];
            $f['out'] = $fileOutput[$i];
            $files[] = $f;
        }

        return $this->render('task/details.html.twig', array(
            'task' => $task,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'limits' => $limits,
            'files' => $files
        ));
    }

    /**
     * @Route("/{id}", name="task_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Task $task)
    {
        $form = $this->createDeleteForm($task);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($task);
            $em->flush();
        }

        return $this->redirectToRoute('management_index');
    }

    /**
     * @param Task $task The Task entity
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Task $task)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('task_delete', array('id' => $task->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
