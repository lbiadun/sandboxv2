<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Sandbox;
use AppBundle\Form\Type\SandboxType;

/**
 * Task controller.
 *
 * @Route("/sandbox")
 */
class SandboxController extends Controller
{

    /**
     * @Route("/new", name="sandbox_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $sandbox = new Sandbox();
        $form = $this->createForm(new SandboxType(), $sandbox);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($sandbox);
            $em->flush();

            return $this->redirectToRoute('management_index', array('id' => $sandbox->getId()));
        }

        return $this->render('sandbox/create.html.twig', array(
            'sandbox' => $sandbox,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/{id}/edit", name="sandbox_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Sandbox $sandbox)
    {
        $deleteForm = $this->createDeleteForm($sandbox);
        $editForm = $this->createForm(new  SandboxType(), $sandbox);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($sandbox);
            $em->flush();

            return $this->redirectToRoute('management_index', array('id' => $sandbox->getId()));
        }
        
        return $this->render('sandbox/details.html.twig', array(
            'sandbox' => $sandbox,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Route("/{id}", name="sandbox_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Sandbox $sandbox)
    {
        $form = $this->createDeleteForm($sandbox);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($sandbox);
            $em->flush();
        }

        return $this->redirectToRoute('management_index');
    }

    /**
     * @param Task $task The Task entity
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Sandbox $sandbox)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('sandbox_delete', array('id' => $sandbox->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
