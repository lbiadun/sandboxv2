<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Language;
use AppBundle\Entity\Task;

/**
 * Language controller.
 *
 * @Route("/management")
 */
class ManagementController extends Controller
{
    /**
     * Dashboard.
     *
     * @Route("/", name="management_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $languages = $em->getRepository('AppBundle:Language')->findAll();
        $sandboxes = $em->getRepository('AppBundle:Sandbox')->findAll();
        $tasks = $em->getRepository('AppBundle:Task')->findAll();
        $judges = $em->getRepository('AppBundle:Judge')->findAll();

        return $this->render('management/index.html.twig', array(
            'languages' => $languages,
            'tasks' => $tasks,
            'judges' => $judges,
            'sandboxes' => $sandboxes
        ));
    }

}

