<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Language;
use AppBundle\Form\Type\LanguageType;

/**
 * Task controller.
 *
 * @Route("/language")
 */
class LanguageController extends Controller
{

    /**
     * @Route("/new", name="language_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $language = new Language();
        $form = $this->createForm(new LanguageType(), $language);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($language);
            $em->flush();

            return $this->redirectToRoute('management_index', array('id' => $language->getId()));
        }

        return $this->render('language/create.html.twig', array(
            'language' => $language,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/{id}/edit", name="language_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Language $language)
    {
        $deleteForm = $this->createDeleteForm($language);
        $editForm = $this->createForm(new LanguageType(), $language);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($language);
            $em->flush();

            return $this->redirectToRoute('management_index', array('id' => $language->getId()));
        }
        
        return $this->render('language/details.html.twig', array(
            'language' => $language,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Route("/{id}", name="language_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Language $language)
    {
        $form = $this->createDeleteForm($language);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($language);
            $em->flush();
        }

        return $this->redirectToRoute('management_index');
    }

    /**
     * @param Task $task The Task entity
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Language $language)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('language_delete', array('id' => $language->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
