<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="result")
 */
class Result
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(name="ip", type="string", length=45)
     */
    protected $ip;

    /**
     * @ORM\ManyToOne(targetEntity="Sandbox", inversedBy="results")
     * @ORM\JoinColumn(name="sandbox_id", referencedColumnName="id")
     */
    protected $sandbox;

    /**
     * @ORM\Column(name="uuid", type="string", length=45)
     */
    protected $uuid;

    /**
     * @ORM\ManyToOne(targetEntity="Language")
     * @ORM\JoinColumn(name="language_id", referencedColumnName="id")
     */
    protected $language;
    
    /**
     * @ORM\ManyToOne(targetEntity="Task", inversedBy="results")
     * @ORM\JoinColumn(name="task_id", referencedColumnName="id")
     */
    protected $task;
    
    /**
     * @ORM\Column(type="integer", options={"default" = -1})
     */
    protected $result;
    
    /**
     * @ORM\Column(type="integer", options={"default" = -1})
     */
    protected $rate;
    
    /**
     * @ORM\Column(type="text")
     */
    protected $times;

    /**
     * @ORM\Column(type="text")
     */
    protected $code;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * @ORM\OneToMany(targetEntity="ResultFile", mappedBy="result")
     */
    protected $resultFiles;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->resultFiles = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ip
     *
     * @param string $ip
     * @return Result
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string 
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set uuid
     *
     * @param string $uuid
     * @return Result
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get uuid
     *
     * @return string 
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Set result
     *
     * @param integer $result
     * @return Result
     */
    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }

    /**
     * Get result
     *
     * @return integer 
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Set sandbox
     *
     * @param \AppBundle\Entity\Sandbox $sandbox
     * @return Result
     */
    public function setSandbox(\AppBundle\Entity\Sandbox $sandbox = null)
    {
        $this->sandbox = $sandbox;

        return $this;
    }

    /**
     * Get sandbox
     *
     * @return \AppBundle\Entity\Sandbox 
     */
    public function getSandbox()
    {
        return $this->sandbox;
    }

    /**
     * Set language
     *
     * @param \AppBundle\Entity\Language $language
     * @return Result
     */
    public function setLanguage(\AppBundle\Entity\Language $language = null)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return \AppBundle\Entity\Language 
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set task
     *
     * @param \AppBundle\Entity\Task $task
     * @return Result
     */
    public function setTask(\AppBundle\Entity\Task $task = null)
    {
        $this->task = $task;

        return $this;
    }

    /**
     * Get task
     *
     * @return \AppBundle\Entity\Task 
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * Add resultFiles
     *
     * @param \AppBundle\Entity\ResultFile $resultFiles
     * @return Result
     */
    public function addResultFile(\AppBundle\Entity\ResultFile $resultFiles)
    {
        $this->resultFiles[] = $resultFiles;

        return $this;
    }

    /**
     * Remove resultFiles
     *
     * @param \AppBundle\Entity\ResultFile $resultFiles
     */
    public function removeResultFile(\AppBundle\Entity\ResultFile $resultFiles)
    {
        $this->resultFiles->removeElement($resultFiles);
    }

    /**
     * Get resultFiles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getResultFiles()
    {
        return $this->resultFiles;
    }

    /**
     * Set rate
     *
     * @param integer $rate
     * @return Result
     */
    public function setRate($rate)
    {
        $this->rate = $rate;

        return $this;
    }

    /**
     * Get rate
     *
     * @return integer 
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * Set times
     *
     * @param string $times
     * @return Result
     */
    public function setTimes($times)
    {
        $this->times = $times;

        return $this;
    }

    /**
     * Get times
     *
     * @return string 
     */
    public function getTimes()
    {
        return $this->times;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Result
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Result
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }
}
