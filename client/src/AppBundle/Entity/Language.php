<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="language")
 */
class Language
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(name="name", type="string", length=45)
     */
    protected $name;
    
    /**
     * @ORM\Column(name="makefile", type="text")
     */
    protected $makefile;

    /**
     * @ORM\Column(name="mode", type="integer")
     */
    protected $mode;

    /**
     * @ORM\Column(name="install", type="string", length=45)
     */
    protected $install;

    /**
     * @ORM\Column(name="main_file_name", type="string", length=45)
     */
    protected $mainFileName;
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Language
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set makefile
     *
     * @param string $makefile
     * @return Language
     */
    public function setMakefile($makefile)
    {
        $this->makefile = $makefile;

        return $this;
    }

    /**
     * Get makefile
     *
     * @return string 
     */
    public function getMakefile()
    {
        return $this->makefile;
    }

    /**
     * Set mode
     *
     * @param integer $mode
     * @return Language
     */
    public function setMode($mode)
    {
        $this->mode = $mode;

        return $this;
    }

    /**
     * Get mode
     *
     * @return integer 
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * Set install
     *
     * @param string $install
     * @return Language
     */
    public function setInstall($install)
    {
        $this->install = $install;

        return $this;
    }

    /**
     * Get install
     *
     * @return string 
     */
    public function getInstall()
    {
        return $this->install;
    }

    /**
     * Set mainFileName
     *
     * @param string $mainFileName
     * @return Language
     */
    public function setMainFileName($mainFileName)
    {
        $this->mainFileName = $mainFileName;

        return $this;
    }

    /**
     * Get mainFileName
     *
     * @return string 
     */
    public function getMainFileName()
    {
        return $this->mainFileName;
    }
}
