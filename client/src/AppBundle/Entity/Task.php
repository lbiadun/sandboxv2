<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="task")
 */
class Task
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(name="title", type="string", length=45)
     */
    protected $title;
    
    /**
     * @ORM\Column(name="content", type="text")
     */
    protected $content;

    /**
     * @ORM\ManyToOne(targetEntity="Judge")
     * @ORM\JoinColumn(name="judge_id", referencedColumnName="id")
     */
    protected $judge;
 
    /**
     * @ORM\Column(name="limits", type="text")
     */
    protected $limits;
    
    /**
     * @ORM\OneToMany(targetEntity="InputFile", mappedBy="task")
     */
    protected $inputFiles;

    /**
     * @ORM\OneToMany(targetEntity="OutputFile", mappedBy="task")
     */
    protected $outputFiles;
    
    /**
     * @ORM\OneToMany(targetEntity="Result", mappedBy="task")
     */
    protected $results;
    
    
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->inputFiles = new \Doctrine\Common\Collections\ArrayCollection();
        $this->outputFiles = new \Doctrine\Common\Collections\ArrayCollection();
        $this->results = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Task
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set limits
     *
     * @param string $limits
     * @return Task
     */
    public function setLimits($limits)
    {
        $this->limits = $limits;

        return $this;
    }

    /**
     * Get limits
     *
     * @return string 
     */
    public function getLimits()
    {
        return $this->limits;
    }

    /**
     * Add inputFiles
     *
     * @param \AppBundle\Entity\InputFile $inputFiles
     * @return Task
     */
    public function addInputFile(\AppBundle\Entity\InputFile $inputFiles)
    {
        $this->inputFiles[] = $inputFiles;

        return $this;
    }

    /**
     * Remove inputFiles
     *
     * @param \AppBundle\Entity\InputFile $inputFiles
     */
    public function removeInputFile(\AppBundle\Entity\InputFile $inputFiles)
    {
        $this->inputFiles->removeElement($inputFiles);
    }

    /**
     * Get inputFiles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getInputFiles()
    {
        return $this->inputFiles;
    }

    /**
     * Add outputFiles
     *
     * @param \AppBundle\Entity\OutputFile $outputFiles
     * @return Task
     */
    public function addOutputFile(\AppBundle\Entity\OutputFile $outputFiles)
    {
        $this->outputFiles[] = $outputFiles;

        return $this;
    }

    /**
     * Remove outputFiles
     *
     * @param \AppBundle\Entity\OutputFile $outputFiles
     */
    public function removeOutputFile(\AppBundle\Entity\OutputFile $outputFiles)
    {
        $this->outputFiles->removeElement($outputFiles);
    }

    /**
     * Get outputFiles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOutputFiles()
    {
        return $this->outputFiles;
    }

    /**
     * Add results
     *
     * @param \AppBundle\Entity\Result $results
     * @return Task
     */
    public function addResult(\AppBundle\Entity\Result $results)
    {
        $this->results[] = $results;

        return $this;
    }

    /**
     * Remove results
     *
     * @param \AppBundle\Entity\Result $results
     */
    public function removeResult(\AppBundle\Entity\Result $results)
    {
        $this->results->removeElement($results);
    }

    /**
     * Get results
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getResults()
    {
        return $this->results;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Task
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set task
     *
     * @param \AppBundle\Entity\Judge $task
     * @return Task
     */
    public function setTask(\AppBundle\Entity\Judge $task = null)
    {
        $this->task = $task;

        return $this;
    }

    /**
     * Get task
     *
     * @return \AppBundle\Entity\Judge 
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * Set judge
     *
     * @param \AppBundle\Entity\Judge $judge
     * @return Task
     */
    public function setJudge(\AppBundle\Entity\Judge $judge = null)
    {
        $this->judge = $judge;

        return $this;
    }

    /**
     * Get judge
     *
     * @return \AppBundle\Entity\Judge 
     */
    public function getJudge()
    {
        return $this->judge;
    }
}
