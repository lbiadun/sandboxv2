<?php
namespace AppBundle\Judge;

use AppBundle\Judge\JudgeAdapterInterface;
use AppBundle\Entity\Result;

class MainJudge extends JudgeAdapter
{
	private $rates = array(
        -1=> "Unknow",
        0 => "All correct",
        1 => "Stdout not correct"
    );

	public function process(Result $result)
	{
        $outputFiles = $result->getTask()->getOutputFiles()->getValues();
        $resultFiles = $result->getResultFiles()->getValues();
		$result->setRate(0);
        for($i = 0; $i < count($resultFiles); $i++)
        {
            if($resultFiles[$i]->getContent() != $outputFiles[$i]->getContent())
            {
                $result->setRate(1);
                break;
            }
        }

        $em = $this->getEntityManager();
        $em->persist($result);
        $em->flush();
	}

	public function getRate(Result $result)
	{
		return $this->rates[$result->getRate()];
	}
}