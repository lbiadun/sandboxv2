<?php
namespace AppBundle\Judge;

use AppBundle\Entity\Result;
use Doctrine\ORM\EntityManager;

abstract class JudgeAdapter {
	private $em;

	function __construct(EntityManager $em) {
		$this->em = $em;
	}

	function getEntityManager(){
		return $this->em;
	}

	abstract public function process(Result $result);
	abstract public function getRate(Result $result);
}