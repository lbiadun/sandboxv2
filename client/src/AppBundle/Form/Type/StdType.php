<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StdType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('contentIn', 'textarea', array(
                'attr' => array('rows' => 6),
                'required' => false,
            ))
            ->add('contentOut', 'textarea', array(
                'attr' => array('rows' => 6),
            ))
        ;
    }
}
