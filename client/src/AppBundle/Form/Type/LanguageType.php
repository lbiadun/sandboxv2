<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LanguageType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('makefile', 'hidden')
            ->add('mode', 'choice', array(
                'choices'  => array(
                    0 => 'Compile',
                    1 => 'Parse',
                    2 => 'Bytecode',
                    -1=> 'Package'
            )))
            ->add('install')
            ->add('mainFileName')
        ;
    }
}
