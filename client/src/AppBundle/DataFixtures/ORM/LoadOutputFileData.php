<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\OutputFile;

class LoadOutputFileData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
    	$file = new OutputFile();
    	$file->setTask($this->getReference("task-hello-world"));
    	$file->setContent("Hello World\n");

        $manager->persist($file);
        $manager->flush();
        
        $this->addReference("output-file-hello-world", $file);
    }

    public function getOrder()
    {
        return 3;
    }
}