<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Task;

class LoadTaskData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
    	$task = new Task();
    	$task->setTitle("Hello world");
    	$task->setContent("write a program that prints on the screen \"Hello world\"");
    	$task->setLimits("-1,-1,-1,65536,0,-1,7866,1024,65536,-1,-1,786");

        $manager->persist($task);
        $manager->flush();
        
        $this->addReference("task-hello-world", $task);
    }

    public function getOrder()
    {
        return 1;
    }
}