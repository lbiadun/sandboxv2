<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Judge;

class LoadJudgeData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
    	$judge = new Judge();
    	$judge->setName("Output 1:1");
    	$judge->setAdapter("MainJudge");
    	$judge->setDescription("Compare outputs");

    	$manager->persist($judge);
    	$manager->flush();
    	
        $this->addReference("judge", $judge);
    }

    public function getOrder()
    {
        return 1;
    }
}