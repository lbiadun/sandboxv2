<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Language;

class LoadLandguageData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
    	$lang1 = new Language();
    	$lang1->setName("make");
    	$lang1->setInstall("make");
    	$lang1->setMode(-1);
    	$lang1->setMainFileName(" ");
    	$lang1->setMakefile(" ");
        $manager->persist($lang1);
        
        $lang2 = new Language();
        $lang2->setName("GCC");
        $lang2->setInstall("gcc");
        $lang2->setMode(0);
        $lang2->setMainFileName("main.c");
        $lang2->setMakefile("compile:\n\tgcc -o main main.c\n\nrun:\n\t./main");
        $manager->persist($lang2);

        $lang3 = new Language();
        $lang3->setName("C#");
        $lang3->setInstall("mono-devel");
        $lang3->setMode(2);
        $lang3->setMainFileName("main.cs");
        $lang3->setMakefile("compile:\n\tmcs main.cs\n\nrun:\n\tmono main.exe");
        $manager->persist($lang3);

        $lang4 = new Language();
        $lang4->setName("C++");
        $lang4->setInstall("g++");
        $lang4->setMode(0);
        $lang4->setMainFileName("main.cpp");
        $lang4->setMakefile("compile:\n\tg++ main.cpp -o main\n\nrun:\n\t./main");
        $manager->persist($lang4);

        $lang5 = new Language();
        $lang5->setName("Java");
        $lang5->setInstall("openjdk-7-jdk");
        $lang5->setMode(2);
        $lang5->setMainFileName("Main.java");
        $lang5->setMakefile("compile:\n\tjavac Main.java\n\nrun:\n\tjava Main");
        $manager->persist($lang5);

        $lang6 = new Language();
        $lang6->setName("PHP");
        $lang6->setInstall("php5");
        $lang6->setMode(1);
        $lang6->setMainFileName("main.php");
        $lang6->setMakefile("compile:\n\t\n\nrun:\n\tphp main.php");
        $manager->persist($lang6);

        $lang7 = new Language();
        $lang7->setName("Python");
        $lang7->setInstall("python");
        $lang7->setMode(0);
        $lang7->setMainFileName("main.py");
        $lang7->setMakefile("compile:\n\t\n\nrun:\n\tpython main.py");
        $manager->persist($lang7);
        
        $manager->flush();
        $this->addReference("language-make", $lang1);
        $this->addReference("language-gcc", $lang2);
    }

    public function getOrder()
    {
        return 1;
    }
}