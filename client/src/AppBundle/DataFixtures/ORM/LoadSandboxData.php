<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Sandbox;

class LoadSandboxData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
    	for($i = 0; $i < 3; $i++)
    	{
	    	$sandbox = new Sandbox();
	    	$sandbox->setIp("localhost");
	    	$sandbox->setPort(3000 + $i);
	
	        $manager->persist($sandbox);
	        $manager->flush();
	        
	        $this->addReference("sandbox-".$i, $sandbox);
    	}
    }

    public function getOrder()
    {
        return 1;
    }
}