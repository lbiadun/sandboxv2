/*
 * main.cpp
 *
 *  Created on: 23 lut 2016
 *      Author: lukasz
 */

#include <ServerSocket.h>
#include <SandboxException.h>
#include <Manager.h>
#include <Log.h>

#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <err.h>

#include <mysql/cppconn/exception.h>
#include <bsd/libutil.h>

using namespace std;

void usage()
{
	cout << "\nUse: sandbox-manager [OPTION]\n\n"
			"\t- start\t\t\trun manager and sandboxes\n\n"
			"\t- stop\t\t\texit Manager and all sandboxes\n\n"
			"\t- reload\t\tstart - stop\n\n"
			"\t- provision\t\tprepare safe environment\n\n"
			"\t- update-languages\tinstall all requirment languages\n\n"
			"\t- enter-chroot\t\tenter to chroot environment\n\n";
}

int main(int argc, char* argv[])
{
	if(geteuid())
	{
		cout << "You must be root!\n";
		exit(EXIT_FAILURE);
	}

	if(argc < 2)
	{
		usage();
	    exit(EXIT_FAILURE);
	}

	string opt = argv[1];
	Manager *m = new Manager();

	int result = 0;
	if( !opt.compare(OPT_START) )
	{
		result = m->start();
	}
	else if( !opt.compare(OPT_STOP) )
	{
		result = m->stop();
	}
	else if( !opt.compare(OPT_RESTART) )
	{
		result = m->restart();
	}
	else if( !opt.compare(OPT_STATUS) )
	{
		result = m->status();
	}
	else if( !opt.compare(OPT_PROVISION) )
	{
		result = m->provision();
	}
	else if( !opt.compare(OPT_INSTALL_LANG) )
	{
		result = m->installLang();
	}
	else if( !opt.compare(OPT_ENTER_CHROOT) )
	{
		result = m->enterChroot();
	}

	if( result )
	{
		cout << "Something went wrong!" << endl;
		exit(EXIT_FAILURE);
	}
	exit(EXIT_SUCCESS);
}


