all:
	make manager/Debug/makefile
	make sandbox/Debug/makefile
	make test/Debug/makefile


install: copyfiles createdb provision


uinstall:
	sudo rm -rf /etc/sandbox
	sudo rm -rf /var/log/sandbox
	sudo rm -rf /var/run/sandbox
	sudo rm -rf /var/chroot
	sudo rm -rf /var/chroot/sandbox
	sudo rm -rf /usr/sbin/sandbox-manager
	sudo rm -rf /usr/sbin/sandbox


copyfiles: createdir
	sudo cp bin/manager /usr/sbin/sandbox-manager
	sudo cp bin/sandbox /usr/sbin/sandbox
	sudo cp config/sandbox.conf /etc/sandbox/sandbox.conf

createdir:
	sudo mkdir -p /etc/sandbox
	sudo mkdir -p /var/log/sandbox
	sudo mkdir -p /var/run/sandbox
	sudo mkdir -p /var/chroot
	sudo mkdir -p /var/chroot/sandbox

createdb:
	php client/app/console doctrine:database:drop --force
	php client/app/console doctrine:database:create
	php client/app/console doctrine:schema:update --force
	php client/app/console doctrine:fixtures:load

provision:
	#sudo apt-get update
	sudo apt-get install -y default-jre
	sudo apt-get install -y libboost-all-dev
	#sudo apt-get install -y uuid-dev
	sudo apt-get install -y debootstrap
	sudo apt-get install -y apache2
	sudo apt-get install -y mysql-server 
	sudo apt-get install -y php5 libapache2-mod-php5
	sudo apt-get install -y php5-mysql
	sudo debootstrap wily /var/chroot/wily

runclient:
	php client/app/console server:run