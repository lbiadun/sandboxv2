/*
 * Config.h
 *
 *  Created on: 2 mar 2016
 *      Author: lukasz
 */

#ifndef CONFIG_H_
#define CONFIG_H_

#define DEBUG

#include <iostream>

#define GLOBAL_MAX_TIMME_EXEC 5
#define LIMIT_DONT_CAHNGE -2

#define GLOBAL_CONFIG_FILE "/etc/sandbox/sandbox.conf"
#define SANDBOX_EXE "/usr/sbin/sandbox"
#define DEFAUL_PORT 7070
#define LOG_DIR "/var/log/sandbox"
#define PID_DIR "/var/run/sandbox"
#define LOG_FILE LOG_DIR "/sandbox.log"
#define PID_FILE PID_DIR "/sandbox.pid"

#define DB_HOST "localhost"
#define DB_NAME "sandbox"
#define DB_USER "root"
#define DB_PASS ""
#define DB_LANG_TABLE "language"
#define DB_TASK_TABLE "task"
#define DB_INPUT_TABLE "input_file"
#define DB_OUTPUT_TABLE "output_file"
#define DB_SANDBOX_TABLE "sandbox"
#define DB_RESULT_TABLE "result"

#define DB_FIELD_MAKE "makefile"
#define DB_FIELD_MAIN_FILE_NAME "main_file_name"
#define DB_FIELD_MODE "mode"
#define DB_FIELD_LIMITS "limits"
#define DB_FIELD_ID "id"
#define DB_FIELD_INPUT_TASKID "task_id"
#define DB_FIELD_INPUT_CONTENT "content"
#define DB_FIELD_ACTIVE "active"
#define DB_FIELD_RUN "run"
#define DB_FIELD_IP "ip"
#define DB_FIELD_PORT "port"
#define DB_FIELD_PID "pid"
#define DB_FIELD_INSTALL "install"
#define DB_FIELD_NAME "name"
#define DB_FIELD_SANDBOX_ID "sandbox_id"
#define DB_FIELD_UUID "uuid"
#define DB_FIELD_LANGUAGE_ID "language_id"
#define DB_FIELD_TASK_ID "task_id"
#define DB_FIELD_RESULT "result"
#define DB_FIELD_RESULT_ID "result_id"
#define DB_FIELD_OUTPUT_CONTENT "content"
#define DB_FIELD_RESULT_CONTENT "content"

#define MAKEFILE "makefile"
#define FILE_STDIN "stdin"
#define FILE_STDOUT "stdout"

#define MAKE "/usr/bin/make"
#define FILE_RESULT "result"
#define FILE_ERROR "error"

#define MAX_FILE_SIZE 2048

const int MAXHOSTNAME = 200;
const int MAXCONNECTIONS = 5;
const int MAXRECV = MAX_FILE_SIZE;

#define MSG_OK 0
#define MSG_ERR_COMP 1
#define MSG_ERR_EXEC 2
#define MSG_ERR_OUTPUT 3

const std::string MESSAGES[] = {
		"Ok",
		"Compile error",
		"Execute error",
		"Output error"

};

#define TMP_DIR "/tmp"
#define STDIN_FILE "stdin"
#define STDOUT_FILE "stdout"
#define STATUS_FILE ".status"
#define CONFIG_FILE ".config"

#define JSON_ID_TASK "task_id"
#define JSON_CODE "code"
#define JSON_LANG "language"
#define JSON_MODE "mode"
#define JSON_COUNT "count"
#define JSON_LIMITS "limits"
#define JSON_STATUS "status"
#define JSON_MESSAGE "msg"
#define JSON_TIME "time"
#define JSON_PORT "port"
#define JSON_DB "database"
#define JSON_HOST "host"
#define JSON_USER "user"
#define JSON_PASS "password"
#define JSON_NAME "name"
#define JSON_SESSION "session"
#define JSON_RESULT_ID "result_id"
#define JSON_STATUS "status"

#define OPT_START "start"
#define OPT_STOP "stop"
#define OPT_RESTART "restart"
#define OPT_STATUS "status"
#define OPT_PROVISION "provision"
#define OPT_INSTALL_LANG "update-languages"
#define OPT_ENTER_CHROOT "enter-chroot"

#define CHROOT_WILY "/var/chroot/wily"
#define CHROOT_SANDBOX "/var/chroot/sandbox"

#endif /* CONFIG_H_ */
