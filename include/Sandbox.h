/*
 * Sandbox.h
 *
 *  Created on: 25 lut 2016
 *      Author: lukasz
 */

#ifndef SANDBOX_H_
#define SANDBOX_H_

#include <Config.h>
#include <ServerSocket.h>
#include <json/json.h>


class Sandbox {
public:

	enum process_mode {
		MODE_COMPILE = 0,
		MODE_INTERPRET = 1,
		MODE_BAITCODE = 2
	};
	struct Task {
		int count;
		process_mode mode;
		int limits[16];
	};

	Sandbox(char* id);
	virtual ~Sandbox();


	bool processRequest(ServerSocket &sock);
	Json::Value loadConfig();
	Task processJsonConfig(Json::Value value);
	bool sendResponse(ServerSocket &sock, int status, int n);
	int processing(Task task);

	/*
	 * Generate output status file in json format
	 */
	bool generateStatusFile(int status, double time[], int n);
	bool generateStatusFile(int status) { return generateStatusFile(status, NULL, 0); }

private:
	/*
	 * Clear tmp directory
	 */
	void clear();

	/*
	 * Get int from Value object
	 */
	int getJsonInteger(Json::Value value, std::string name);

	/*
	 * Start the process that after a certain time determined end another process
	 */
	int startKillTimer(int pid, int time);

	/*
	 * Stop process kill timer
	 */
	void stopKillTimer(int k_pid);

	/*
	 * resource.h enum __rlimit_resource
	 *
	 * RLIMIT_CPU = 0,
	 * RLIMIT_FSIZE = 1,
	 * RLIMIT_DATA = 2,
	 * RLIMIT_STACK = 3,
	 * RLIMIT_CORE = 4,
	 * RLIMIT_RSS = 5,
	 * RLIMIT_NPROC = 6,
	 * RLIMIT_NOFILE = 7,
	 * RLIMIT_MEMLOCK = 8,
	 * RLIMIT_AS = 9,
	 * RLIMIT_LOCKS = 10,
	 * RLIMIT_SIGPENDING = 11,
	 * RLIMIT_MSGQUEUE = 12,
	 * RLIMIT_NICE = 13,
	 * RLIMIT_RTPRIO = 14,
	 * RLIMIT_RTTIME = 15,
	 * RLIMIT_NLIMITS = 16
	 *
	 * Set limits on the process with the specified pid
	 */
	bool setLimits(int pid, int limits[]);

	void compile_init();
	void compile_deinit();

	void run_init(int idx);
	void run_deinit();

	int run(int idx, int limits[]);
	int compile();

	int saved_stdin;
	int saved_stdout;
	int saved_stderr;

	char* id;
	std::string work_dir;

	double * time;
};

#endif /* SANDBOX_H_ */
