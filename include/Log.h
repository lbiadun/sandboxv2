/*
 * Log.h
 *
 *  Created on: 25 mar 2016
 *      Author: lukasz
 */

#ifndef LOG_H_
#define LOG_H_

#include <iostream>
#include <fstream>

class Log {
public:
	Log();
	virtual ~Log();

	static void log(std::string msg);
};

#endif /* LOG_H_ */
