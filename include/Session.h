/*
 * Manager.h
 *
 *  Created on: 10 mar 2016
 *      Author: lukasz
 */

#ifndef SESSION_H_
#define SESSION_H_

#include <iostream>
#include <ServerSocket.h>
#include <ClientSocket.h>
#include <Manager.h>

#include <json/json.h>

#include <mysql/mysql_connection.h>
#include <mysql/mysql_driver.h>
#include <mysql/cppconn/statement.h>
#include <mysql/cppconn/resultset.h>


class Session {
public:
	struct Task
	{
		int id;
		int language;
	    std::string code;
	};
	struct Lang {
		int id;
		std::string mainfile;
		int mode;
	};

	Session(Manager::SandboxCopy &sand, Manager::DbConn db_conn);
	virtual ~Session();

	void processRequest(ServerSocket &sock);

	std::string captureContent(ServerSocket &sock);
	Task processJsonContent(std::string json);
	Lang generateMakefile(Task task);
	int generateFilesInput(Task task);
	bool generateMainFile(Task task, Lang lang);
	bool generateConfigFile(Task task, Lang lang, int inputs);
	bool prepareAndSendPackage(ClientSocket &sock, Lang lang, int n);
	bool captureResponse(ClientSocket &sock);
	bool prepareAndSendResponse(ServerSocket &sock, int result_id);

	int logResult(ServerSocket &sock, Task task, Lang lang, int inputs);

private:
	int port;
	std::string ip;
	int id_sand;

	sql::mysql::MySQL_Driver *driver;
	sql::Connection *con;
	sql::Statement *stmt;
	sql::ResultSet  *res;
	sql::PreparedStatement  *prep_stmt;

	char * uuid;
	std::string tmp_dir;

	std::string getJsonString(Json::Value value, std::string name);
	int getJsonInteger(Json::Value value, std::string name);

	std::string getTime();
};

#endif /* SESSION_H_ */
