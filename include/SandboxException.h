// SandboxException class


#ifndef SandboxException_class
#define SandboxException_class

#include <string>
#include <iostream>

class SandboxException
{
 public:
  SandboxException ( int code, std::string s ) : m_s ( s ), code(code) { };
  SandboxException ( int code, const char * msg1, const char * msg2 ) : code(code) { m_s = msg1; m_s = m_s + " " + msg2; };
  SandboxException ( int code, const char * msg1, const char * msg2, const char * msg3 ) : code(code) { m_s = msg1; m_s = m_s + " " + msg2; m_s = m_s + ": " + msg3; };
  ~SandboxException (){};

  std::string description() { return m_s; }
  void printError() { std::cout << "Exception was caught: " << description() << "\n"; }
  std::string getMsg() { return std::string("Exception nr: ") + std::to_string(code) + " - " + description(); }
  int getCode() { return code; }
 private:

  std::string m_s;
  int code;

};

#endif
