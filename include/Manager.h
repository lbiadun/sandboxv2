/*
 * Manager.h
 *
 *  Created on: 19 mar 2016
 *      Author: lukasz
 */

#ifndef MANAGER_H_
#define MANAGER_H_

#include <ServerSocket.h>
#include <Config.h>

#include <json/json.h>

#include <mysql/mysql_connection.h>
#include <mysql/mysql_driver.h>
#include <mysql/cppconn/statement.h>
#include <mysql/cppconn/resultset.h>

class Manager {
public:
	struct DbConn {
		std::string host;
		std::string name;
		std::string user;
		std::string pass;
	};
	struct SandboxCopy{
		int pid;
		int uuid;
		std::string ip;
		int port;
		bool active;
		bool run;
	};

	Manager();
	virtual ~Manager();

	int start();
	int stop();
	int restart();
	int status();
	int provision();
	int installLang();
	int enterChroot();

	void init();
	void deinit();
	void run();
	void loadConfig();
	void connectingDatabase();
	void loadSandboxConfig();
	void processRequest(ServerSocket &sock);
	bool runSandbox(SandboxCopy &sand);
	bool exitSandbox(SandboxCopy &sand);
	SandboxCopy getFreeSandbox();
	bool notifyStatusSandBox(SandboxCopy &sand);

private:
	int sandbox_count;
	int sandbox_count_curr = 0;
	int last_use = -1;
	SandboxCopy * sandboxes;
	DbConn db_conn;
	int port;

	sql::mysql::MySQL_Driver *driver;
	sql::Connection *con;
	sql::Statement *stmt;
	sql::ResultSet  *res;

	std::string getJsonString(Json::Value value, std::string name);
};

#endif /* MANAGER_H_ */
