/*
 * FileHelper.h
 *
 *  Created on: 2 mar 2016
 *      Author: lukasz
 */

#ifndef FILEHELPER_H_
#define FILEHELPER_H_

#include <iostream>
#include <vector>
#include <Socket.h>

class FileHelper {
public:
	FileHelper(std::string dir, std::string name);
	virtual ~FileHelper();

	void load();
	void save();

	void send(Socket &sock);
	void recv(Socket &sock);

	void zip(std::vector<std::string> files);
	void unzip();

	/* options */
	static const int STDOUT = 1;
	static const int STDIN = 2;

private:
	static void safe_create_dir(const char * );

	std::string name;
	std::string dir;
	char * memblock;
	std::streampos size;
	bool isValid;
};

#endif /* FILETRANSFER_H_ */
