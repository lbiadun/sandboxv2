/*
 * main.cpp
 *
 *  Created on: 23 lut 2016
 *      Author: lukasz
 */

#include <FileHelper.h>
#include <Sandbox.h>
#include <ServerSocket.h>
#include <SandboxException.h>
#include <Log.h>

#include <iostream>
#include <string>

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <mysql/cppconn/exception.h>
#include <uuid/uuid.h>

using namespace std;

void usage()
{
	cout << "Use: sandbox [port]\n";
}

int main(int argc, char* argv[])
{

	if(argc < 2)
	{
		usage();
		return 1;
	}

	if( chdir(CHROOT_SANDBOX) || chroot(".") )
	{
		std::cout << "Something went wrong!";
		return 1;
	}

	cout << "[" << getpid() << "] Sandbox starting... \n";
	int port = atoi(argv[1]);


	try
	{
		ServerSocket server (port);

		while ( true )
		{
			ServerSocket new_sock;
			server.accept ( new_sock );

			uuid_t id;
			uuid_generate(id);

			char* uuid = new char[100];
			uuid_unparse(id, uuid);

			Sandbox s( uuid );
			try
			{
				s.processRequest(new_sock);

			}
			catch ( SandboxException &e )
			{
				Log::log(e.getMsg());
				s.generateStatusFile( 3 );
				s.sendResponse(new_sock, 0, -1);
				//e.printError();
			}
		}
	} catch ( SandboxException& e ) {
		e.printError();
	}
	return 0;
}

