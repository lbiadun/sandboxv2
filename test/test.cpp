/*
 * test.cpp
 *
 *  Created on: 22 mar 2016
 *      Author: lukasz
 */
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE Hello
#include <boost/test/unit_test.hpp>

#include <iostream>
#include <fstream>

#include <FileHelper.h>
#include <ClientSocket.h>
#include <Config.h>
#include <json/json.h>

#define DIR "/home/lukasz/sandboxv2/test/examples/"
#define IP "localhost"
#define PORT 30000

int runSandbox(std::string file)
{
	FileHelper *fh = new FileHelper(DIR, file);
	fh->load();

	ClientSocket client_sock(IP, PORT);
	fh->send(client_sock);

	fh = new FileHelper(DIR, "response.zip");
	fh->recv(client_sock);
	fh->save();
	fh->unzip();

	std::ifstream status_file(DIR "/" STATUS_FILE);
	Json::Value root;
	Json::Reader reader;
	reader.parse(status_file, root);
	return root.get(JSON_STATUS, -1).asInt();
}

void clear()
{
	system("rm -rf .status response.zip stdout*");
}

BOOST_AUTO_TEST_SUITE( Sandbox )
BOOST_AUTO_TEST_CASE( allOk )
{
    BOOST_CHECK( runSandbox("package_ok.zip") == 0 );
    clear();
}

BOOST_AUTO_TEST_CASE( compileError )
{
    BOOST_CHECK( runSandbox("package_compile_error.zip") == 1 );
    clear();
}
/*BOOST_AUTO_TEST_CASE( runError )
{
    BOOST_CHECK( runSandbox("package_run_error.zip") == 2 );
    clear();
}*/
BOOST_AUTO_TEST_CASE( notConfigFile )
{
    BOOST_CHECK( runSandbox("package_not_config.zip") == 3 );
    clear();
}
BOOST_AUTO_TEST_CASE( notZip )
{
    BOOST_CHECK( runSandbox("package_not_zip.zip") == 3 );
    clear();
}
BOOST_AUTO_TEST_CASE( jsonLack_Field )
{
    BOOST_CHECK( runSandbox("package_json_not_count.zip") == 3 );
    clear();
}
BOOST_AUTO_TEST_CASE( jsonParseError )
{
    BOOST_CHECK( runSandbox("package_json_parse_error.zip") == 3 );
    clear();
}
BOOST_AUTO_TEST_CASE( notMainFile )
{
    BOOST_CHECK( runSandbox("package_not_main_file.zip") == 1 );
    clear();
}
BOOST_AUTO_TEST_CASE( notMakefile )
{
    BOOST_CHECK( runSandbox("package_not_makefile.zip") == 3 );
    clear();
}
BOOST_AUTO_TEST_CASE( notStdinFile )
{
    BOOST_CHECK( runSandbox("package_not_stdin_file.zip") == 0 );
    clear();
}
BOOST_AUTO_TEST_CASE( timeError )
{
    BOOST_CHECK( runSandbox("package_time_error.zip") == 2 );
    clear();
}
BOOST_AUTO_TEST_CASE( limitError )
{
    BOOST_CHECK( runSandbox("package_limit_error.zip") == 3 );
    clear();
}

BOOST_AUTO_TEST_SUITE_END()


