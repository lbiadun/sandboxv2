CREATE DATABASE `sandbox` /*!40100 DEFAULT CHARACTER SET utf8 */;

CREATE TABLE `input_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `task_id` (`task_id`),
  CONSTRAINT `task_id` FOREIGN KEY (`task_id`) REFERENCES `task` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `languages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `makefile` text NOT NULL,
  `mode` varchar(45) NOT NULL,
  `install` varchar(45) NOT NULL,
  `main_file_name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

CREATE TABLE `sandbox` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(45) NOT NULL,
  `port` int(11) NOT NULL,
  `pid` int(11) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `run` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `port_UNIQUE` (`port`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

CREATE TABLE `task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text NOT NULL,
  `limits` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

CREATE TABLE `sandbox`.`result` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `ip` VARCHAR(45) NOT NULL,
  `sandbox_id` INT NOT NULL,
  `uuid` VARCHAR(45) NOT NULL,
  `languages_id` INT NOT NULL,
  `task_id` INT NOT NULL,
  `result` INT NOT NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `sandbox`.`output_file` (
  `id` INT NOT NULL,
  `task_id` INT NOT NULL,
  `content` TEXT NOT NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `sandbox`.`result_file` (
  `id` INT NOT NULL,
  `result_id` INT NOT NULL,
  `content` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));
